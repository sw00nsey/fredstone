package com.mygdx.fs;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.fredstone.FredStoneGame;

import android.os.Bundle;

public class AndroidLauncher extends AndroidApplication
{
  @Override
  protected void onCreate( Bundle savedInstanceState )
  {
    super.onCreate( savedInstanceState );
    AndroidApplicationConfiguration config = new AndroidApplicationConfiguration( );
    initialize( new FredStoneGame( ), config );
  }
}
