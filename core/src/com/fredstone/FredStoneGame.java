package com.fredstone;

import com.badlogic.gdx.Game;
import com.fredstone.actor.CardActor;
import com.fredstone.client.KNClient;
import com.fredstone.screens.CardCollection;
import com.fredstone.screens.Login;
import com.fredstone.screens.ScreenManager;
import com.fredstone.tween.CardActorAccessor;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

public class FredStoneGame extends Game
{
  public static KNClient client;
  private static TweenManager tweenManager;
  // private Player player;

  @Override
  public void create( )
  {
    ScreenManager.init( this );
    client = new KNClient( );
    setTweenManager( new TweenManager( ) );
    Tween.setCombinedAttributesLimit( 4 ); // default is 3, yet for rgba color
                                           // setting we need to raise to 4!
    Tween.registerAccessor( CardActor.class, new CardActorAccessor( ) );

    this.setScreen( new Login( ) );
  }

  @Override
  public void render( )
  {
    super.render( );
  }

  @Override
  public void dispose( )
  {
    client.removeUserFromQueue( );
  }
  
  /**
   * method to get the {@link TweenManager} instance
   * @return {@link TweenManager}
   */
  public static TweenManager getTweenManager( )
  {
    return tweenManager;
  }
  
  /**
   * method to set the {@link TweenManager} instance
   * @param tweenManager
   */
  public static void setTweenManager( TweenManager tweenManager )
  {
    FredStoneGame.tweenManager = tweenManager;
  }

  public void test( )
  {

  }

}
