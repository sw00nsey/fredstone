
package com.fredstone.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.fredstone.FredStoneGame;
import com.fredstone.model.Card;
import com.fredstone.model.Player;
import com.fredstone.model.TurnData;
import com.fredstone.screens.ScreenOfCardgroup;
import com.fredstone.tween.CardActorAccessor;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;

public class CardActor extends Actor
{

  private static final float SCALE = 0.4f;
  public static float WIDTH = 100 * SCALE;
  public static float HEIGHT = 135.75f * SCALE;

  private static final Vector3 tp = new Vector3( );

  private Label attackLbl, hpLbl, costLbl;
  private BitmapFont font;
  private LabelStyle labelStyle;

  private ScreenOfCardgroup board;
  private static Sprite sprite = new Sprite( new Texture( Gdx.files.internal( "blankhs.png" ) ) );
  private static Sprite oppSprite = new Sprite( new Texture( Gdx.files.internal( "cardback.png" ) ) );

  private boolean touchable, ownActor;

  public Sprite getSprite( )
  {
    return sprite;
  }

  public void setSprite( Sprite sprite )
  {
    this.sprite = sprite;
  }

  private int number;
  private CardActorGroup cardGroup;
  private Card card;
  private int handIndex;

  private int cardType;

  public CardActor( ScreenOfCardgroup board, Card card, boolean touchable )
  {
    this.board = board;
    this.card = card;

    this.touchable = touchable;

    setBounds( 0, 0, WIDTH, HEIGHT );
    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).setScale( 0.07f );
    labelStyle = new LabelStyle( font, Color.WHITE );

    // attackLbl = new Label(""+card.getAttack(), labelStyle);
    final CardActor cardActor = this;

    this.addListener( new InputListener( )
    {
      @Override
      public void touchUp( InputEvent event, float x, float y, int pointer, int button )
      {
        tp.set( Gdx.input.getX( ), Gdx.input.getY( ), 0 );
        getStage( ).getCamera( ).unproject( tp );
        cardActor.updateGroup( tp.x, tp.y );
        Vector2 pos = cardGroup.getPosition( cardActor );
        if( pos == null )
        {

        }
        moveTo( pos.x, pos.y );
        System.out.println( "touch up" );
      }

      @Override
      public boolean touchDown( InputEvent event, float x, float y, int pointer, int button )
      {
        System.out.println( get_touchable( ) );
        if( get_touchable( ) )
        {
          if( cardGroup.isHand( ) )
          {
            System.out.println( "touchDown" );
            handIndex = cardGroup.getCardActorPosition( cardActor );
            System.out.println( "[CardActor]: Hand index " + handIndex );
            return true;
          }
          else if( cardGroup.isOwnBoard( ) )
          {
            FredStoneGame.client.setOwnBoardIndex( cardGroup.getCardActorPosition( cardActor ) );
            return true;
          }
        }
        else
        {
          if( cardGroup.isBoard( ) )
          {
            FredStoneGame.client.setOpponentBoardIndex( cardGroup.getCardActorPosition( cardActor ) );
            System.out.println( "opponent board index: " + cardGroup.getCardActorPosition( cardActor ) );
          }
        }
        return false;
      }

      public void touchDragged( InputEvent event, float x, float y, int pointer )
      {
        tp.set( Gdx.input.getX( ), Gdx.input.getY( ), 0 );
        getStage( ).getCamera( ).unproject( tp );
        moveTo( tp.x - WIDTH / 2, tp.y - HEIGHT / 2 );
      }

      @Override
      public void enter( InputEvent event, float x, float y, int pointer, Actor fromActor )
      {

      }

      @Override
      public void exit( InputEvent event, float x, float y, int pointer, Actor fromActor )
      {
        // System.out.println("exit");
      }
    } );
  }

  @Override
  public void setSize( float width, float height )
  {
    super.setSize( width, height );
    this.WIDTH = width;
    this.HEIGHT = height;
  }

  /**
   * method to update the {@link CardActorGroup}
   * @param x
   * @param y
   */
  public void updateGroup( float x, float y )
  {
    CardActorGroup group = board.groupContainsPoint( x, y );
    if( group != null && !group.equals( cardGroup ) )
    {
      if( group.isOwnBoard( ) && group.getTouchable( ) )
      {

        int index = group.getIndexByPosition( x, y );
        TurnData data = new TurnData( );
        data.setBoardIndex( index );
        System.out.println( "[CardActor]: Boardindex: " + index );
        data.setHandIndex( handIndex );
        System.out.println( "[CardActor]: Handindex: " + handIndex );
        data.setCardID( card.getID( ) );
        data.setManaCrystals( card.getCost( ) );
        System.out.println( card.getCost( ) );
        FredStoneGame.client.sendPlayCard( data );
        System.out.println( "index " + index );

        // if (FredStoneGame.client.isValidAction() == true) {
        // cardGroup.remove(this);
        // group.add(this, Math.min(group.getListSize(), index));
        //
        // }
      }
      else if( !group.getTouchable( ) )
        System.out.println( "Not touchable!" );
      else
        System.out.println( "Wrong group!" );

    }
  }

  /**
   * method to move the {@link CardActor} to the indicated position
   * @param x
   * @param y
   */
  public void moveTo( float x, float y )
  {
    setPosition( x, y );
    // CGB.getTweenManager().killTarget();
    Tween.to( this, CardActorAccessor.POS_XY, 0.8f ).target( x, y ).ease( TweenEquations.easeNone )
        .start( FredStoneGame.getTweenManager( ) );

  }

  /**
   * method to move the {@link CardActor} to the indicated position
   * @param position
   */
  public void moveTo( Vector2 position )
  {
    moveTo( position.x, position.y );
  }

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (sprite == null) {
			// do nothing!
		} else if(card == null) {
			batch.draw(oppSprite, getX(), getY(), WIDTH, HEIGHT);
		} else {
			batch.draw(sprite, getX(), getY(), WIDTH, HEIGHT);
			font.setUseIntegerPositions(false);
			font.draw(batch, "" + card.getCost(), getX() + WIDTH / 8, getY() + HEIGHT - (font.getLineHeight() / 2));
			font.draw(batch, "" + card.getName(), getX() + WIDTH / 8, getY() + HEIGHT / 2);
			switch (card.getCardType()) {
			case MONSTER:
				font.draw(batch, "" + card.getAttack(), getX() + WIDTH / 8, getY() + HEIGHT / 6);
				font.draw(batch, "" + card.getDefense(), getX() + WIDTH - (font.getSpaceWidth() * 3),
						getY() + HEIGHT / 6);
				break;
			case SPELL:
	
				break;
			default:
				break;
			}
		}
	}
  /**
   * method to set the {@link CardActorGroup}
   * @param cardGroup
   */
  public void setGroup( CardActorGroup cardGroup )
  {
    this.cardGroup = cardGroup;
  }

  @Override
  public void act( float delta )
  {
    super.act( delta );
  }

  /**
   * method to get the {@link CardActor} card
   * @return {@link Card}
   */
  public Card getCard( )
  {
    return card;
  }

  /**
   * method to set the {@link CardActor} card
   * @param {@link Card} card
   */
  public void setCard( Card card )
  {
    this.card = card;
  }

  public void dispose( )
  {
    sprite.getTexture( ).dispose( );
  }

  /**
   * method to get the {@link CardActor} number
   * @return {@link CardActor} - number
   */
  public int getNumber( )
  {
    return number;
  }

  /**
   * method to set the {@link CardActor} - number
   * @param {@link Integer} number
   */
  public void setNumber( int number )
  {
    this.number = number;
  }

  /**
   * method to toggle the touchable value
   */
  public void toggleTouchable( )
  {
    this.touchable = !this.touchable;
  }

  /**
   * method to set the touchable value
   * @param touchable
   */
  public void setTouchable( boolean touchable )
  {
    this.touchable = touchable;
  }

  /**
   * method to get the touchable value
   * @return {@link CardActor} - touchable
   */
  public boolean get_touchable( )
  {
    return touchable;
  }
}
