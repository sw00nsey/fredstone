package com.fredstone.actor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class CardActorGroup
{

  private List<CardActor> list;
  private final float X, Y, WIDTH, HEIGHT;
  private static final int MARGIN = 2;
  private static final Vector2 position = new Vector2( );
  private static final Rectangle rect = new Rectangle( );
  private boolean touchable, ownGroup;
  private String id;

  public CardActorGroup( float x, float y, float width, float height, String id, boolean touchable )
  {
    this.list = new ArrayList<CardActor>( );
    this.X = x;
    this.Y = y;
    this.WIDTH = width;
    this.HEIGHT = height;
    this.touchable = touchable;
    this.ownGroup = touchable;
    this.id = id;
  }

  /**
   * method to get the {@link CardActorGroup} - {@link Rectangle}
   * @return {@link Rectangle}
   */
  public Rectangle getRect( )
  {
    return rect.set( X, Y, WIDTH, HEIGHT );
  }

  /**
   * method to update all {@link CardActor} positions
   */
  private void updatePositions( )
  {
    for( CardActor c : list )
    {
      getPosition( c );
      c.moveTo( position.x, position.y );
    }
  }

  /**
   * method to add a card actor to the specific position
   * @param card
   * @param index
   */
  public void add( CardActor card, int index )
  {
    try
    {
      list.add( index, card );
    }
    catch( IndexOutOfBoundsException e )
    {
      list.add( list.size( ), card );
    }
    System.out.println( "add" );
    card.setGroup( this );
    updatePositions( );
  }

  /**
   * method to remove the {@link CardActor} from the {@link CardActorGroup}
   * @param card
   */
  public void remove( CardActor card )
  {
    list.remove( card );
    // card.dispose( );
    updatePositions( );
  }

  /**
   * method to remove the {@link CardActor} from the {@link CardActorGroup}
   * @param index
   */
  public void remove( int index )
  {
    remove( list.get( index ) );
    // list.remove(index);
    // updatePositions();
  }

  /**
   * method to get the position of the {@link CardActor}
   * @param card
   * @return {@link Vector2}
   */
  public Vector2 getPosition( CardActor card )
  {
    if( list.contains( card ) )
    {
      return getPosition( list.indexOf( card ) );
    }
    else
    {
      return null;
    }
  }

  /**
   * method to get the {@link CardActor}
   * @param index
   * @return {@link CardActor}
   */
  public CardActor getCardActorByIndex( int index )
  {
    return list.get( index );
  }

  /**
   * method to get the {@link CardActor} position
   * @param actor
   * @return {@link Integer}
   */
  public int getCardActorPosition( CardActor actor )
  {
    return list.indexOf( actor );
  }

  /**
   * method to get the index of the {@link CardActor} by position
   * @param posx
   * @param y
   * @return {@link Integer}
   */
  public int getIndexByPosition( float posx, float y )
  {
    System.out.println( "[CardActorGroup]: getIndexByPosition listSize: " + getListSize( ) );
    if( getListSize( ) == 0 )
      return 0;
    float listWidth = CardActor.WIDTH * getListSize( ) + MARGIN * ( getListSize( ) - 1 );
    float deltaX = posx - ( X + WIDTH / 2 - listWidth / 2 );
    System.out.println( "POSX: " + posx );
    System.out.println( "(X+WIDTH/2 - listWidth/2) : " + ( X + WIDTH / 2 - listWidth / 2 ) );
    System.out.println( "DeltaX: " + deltaX );
    if( deltaX < 0 )
    {
      return 0;
    }
    else if( deltaX > listWidth )
    {
      return list.size( ) + 1;
    }
    else
    {
      return ( int ) Math.floor( deltaX * ( getListSize( ) + 1 ) / listWidth );
    }
  }

  /**
   * method to get the {@link Vector2} position by index
   * @param index
   * @return {@link Vector2}
   */
  public Vector2 getPosition( int index )
  {

    int size = this.getListSize( );
    float x, y;

    x = this.X + WIDTH / 2 - ( CardActor.WIDTH * size + MARGIN * ( size - 1 ) ) / 2
        + index * ( CardActor.WIDTH + MARGIN );
    y = this.Y + HEIGHT / 2 - CardActor.HEIGHT / 2;

    return position.set( x, y );
  }

  /**
   * method to get the list size
   * @return {@link Integer}
   */
  public int getListSize( )
  {
    return list.size( );
  }

  /**
   * method to get the {@link CardActorGroup} width
   * @return {@link Float}
   */
  public float getWIDTH( )
  {
    return WIDTH;
  }

  /**
   * method to get the {@link CardActorGroup} height
   * @return {@link Float}
   */
  public float getHEIGHT( )
  {
    return HEIGHT;
  }

  /**
   * method to get the {@link CardActorGroup} touchable value
   * @return {@link Boolean}
   */
  public boolean getTouchable( )
  {
    return this.touchable;
  }

  /**
   * method to check if the {@link CardActorGroup} is a hand group
   * @return {@link Boolean}
   */
  public boolean isHand( )
  {
    return id.equals( "hand" );
  }

  /**
   * method to check if the {@link CardActorGroup} is the own hand group
   * @return {@link Boolean}
   */
  public boolean isOwnHand( )
  {
    if( this.ownGroup )
      return id.equals( "hand" );

    return false;
  }

  /**
   * method to check if the {@link CardActorGroup} is a board group
   * @return {@link Boolean}
   */
  public boolean isBoard( )
  {
    return id.equals( "board" );
  }

  /**
   * method to check if the {@link CardActorGroup} is the own board group
   * @return {@link Boolean}
   */
  public boolean isOwnBoard( )
  {
    if( this.ownGroup )
      return id.equals( "board" );

    return false;
  }

  /**
   * method to check if the {@link CardActorGroup} is your own group
   * @return {@link Boolean}
   */
  public boolean isOwnGroup( )
  {
    return ownGroup;
  }

  /**
   * method to set the {@link CardActorGroup} touchable value
   * @param touchable
   */
  public void setTouchable( boolean touchable )
  {
    Iterator<CardActor> it = list.iterator( );
    CardActor actor;

    this.touchable = touchable;

    while( it.hasNext( ) )
    {
      actor = it.next( );
      actor.setTouchable( touchable );
    }
  }

  /**
   * method to toggle the {@link CardActorGroup} touchable value
   */
  public void toggleTouchable( )
  {
    Iterator<CardActor> it = list.iterator( );
    CardActor actor;

    this.touchable = !this.touchable;

    while( it.hasNext( ) )
    {
      actor = it.next( );
      actor.setTouchable( touchable );
    }

    System.out.println( "Touchable toggled." );
  }
}
