package com.fredstone.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.fredstone.actor.CardActorGroup;
import com.fredstone.model.AttackData;
import com.fredstone.model.Card;
import com.fredstone.model.Deck;
import com.fredstone.model.LoginData;
import com.fredstone.model.Player;
import com.fredstone.model.PlayerStats;
import com.fredstone.model.TurnData;
import com.fredstone.model.User;
import com.fredstone.screens.BoardScreen;
import com.fredstone.screens.Login;
import com.fredstone.screens.MainMenu;
import com.fredstone.screens.Register;
import com.fredstone.screens.ScreenManager;
import com.fredstone.utils.CardList;
import com.fredstone.utils.CardType;
import com.fredstone.utils.Message;
import com.fredstone.utils.MessageType;

public class KNClient
{
  private Client client = new Client( );
  private Kryo kryo = client.getKryo( );
  private Player player;
  private User user;
  // private final String SERVER_IP = "192.168.4.194";
  private boolean validAction;

  private BoardScreen board;
  private final String SERVER_IP = "127.0.0.1";
  
  private int ownBoardIndex, opponentBoardIndex;

  public KNClient( )
  {
    client.start( );

    kryo.register( Message.class, 20 );
    kryo.register( MessageType.class, 21 );
    kryo.register( Card.class, 22 );
    kryo.register( Deck.class, 24 );
    kryo.register( Player.class, 25 );
    kryo.register( PlayerStats.class, 26 );
    kryo.register( User.class, 27 );
    kryo.register( Document.class, 28 );
    kryo.register( ArrayList.class, 29 );
    kryo.register( LoginData.class, 31 );
    kryo.register( Connection.class, 32 );
    kryo.register( Connection.class, 33 );
    kryo.register( Deck.class, 34 );
    kryo.register( User.class, 35 );
    kryo.register( ArrayList.class, 36 );
    kryo.register( CardType.class, 37 );
    kryo.register( TurnData.class, 38 );
    kryo.register( CardList.class, 39 );
    kryo.register( AttackData.class, 40 );

    // InetAddress ad = client.discoverHost( 54777, 30000 );
    // System.out.println( ad );

    // System.out.println( "Username: " + user.getName( ) );

    new Thread( client ).start( );

    try
    {
      client.connect( 30000, SERVER_IP, 54555, 54777 );

      client.addListener( new Listener( )
      {
        public void received( Connection co, Object obj )
        {
          if( obj instanceof Message )
          {
            Message msg = ( Message ) obj;

            switch( msg.getMessageType( ) )
            {
              case ATTACK:
                if( msg.getObject( ) instanceof AttackData )
                {
                  AttackData data = ( AttackData ) msg.getObject( );
                  board.updateBoardCards( data );
                }
              break;
              case BEGIN_TURN:
                if( msg.getObject( ) instanceof TurnData )
                {
                  // while( ! board.isActive( ) )
                  // {
                  // try
                  // {
                  // Thread.sleep( 600 );
                  // }
                  // catch( InterruptedException inEx )
                  // {
                  // inEx.printStackTrace( );
                  // }
                  // }

                  TurnData data = ( TurnData ) msg.getObject( );

                  if( data.getPlayerID( ) == player.getID( ) )
                  {
                    player.setManaCrystals( data.getManaCrystals( ) );
                    board.addHandCard( CardList.getInstance( ).getCard( data.getCardID( ) ) );
                  }
                  else
                  {
                    board.addOpponentHandCard( 1 );
                  }
                  System.out.println( "board toggled" );
                  board.toggleActive( );
                }
              break;
              case CONNECT_TO_SERVER:
                if( msg.getObject( ) instanceof User )
                {
                  user = ( User ) msg.getObject( );

                  System.out.println( "[CLIENT]: User object received." );
                  System.out.println( "[CLIENT]: ID: " + user.getID( ) + " Name: " + user.getName( ) );

                  ScreenManager.getInstance( ).setScreen( new MainMenu( ) );
                }
                else if( msg.getObject( ) instanceof CardList )
                {
                  CardList.init( ( CardList ) msg.getObject( ) );
                }
                else
                  System.out.println( "[CLIENT]: Object is not an user." );
              break;
              case CONNECT_TO_ROOM:
                if( msg.getObject( ) instanceof Integer )
                {
                  int id = ( Integer ) msg.getObject( );
                  player.setRoomID( id );
                }
              break;
              case CONNECT_TO_ROOM_ERROR:
                System.out.println( ( String ) msg.getObject( ) );
              break;
              case END_TURN:
                board.toggleActive( );
              break;
              case LOGIN_ERROR:
                System.out.println( ( String ) msg.getObject( ) );
              break;
              case PLAYER_OBJ:
                if( msg.getObject( ) instanceof Player )
                {
                  player = ( Player ) msg.getObject( );
                }
              break;
              case START_GAME:
                if( msg.getObject( ) instanceof Player )
                {
                  Player opp = ( Player ) msg.getObject( );
                  System.out.println( opp.getName( ) );
                  board = new BoardScreen( player, opp );
                  ScreenManager.getInstance( ).setScreen( board );
                }
                else if( msg.getObject( ) instanceof List<?> )
                {
                  @SuppressWarnings( "unchecked" )
                  ArrayList<TurnData> dataList = ( ArrayList<TurnData> ) msg.getObject( );
                  Iterator<TurnData> it = dataList.iterator( );
                  TurnData data;
                  CardActorGroup hand = board.getHand( );

                  while( it.hasNext( ) )
                  {
                    data = it.next( );
                    Card card = CardList.getInstance( ).getCard( data.getCardID( ) );
                    card.setCardType( CardType.MONSTER );
                    System.out.println( card.getName( ) );
                    CardActorGroup b = board.getHand( );

                    System.out.println( b.getListSize( ) );
                    board.addHandCard( card );
                    // hand.add( new CardActor( board, new Texture(
                    // Gdx.files.internal( "blankhs.png" ) ),
                    // card ), hand.getListSize( ) );
                  }
                }
                else if( msg.getObject( ) instanceof Integer )
                {
                  int amount = ( Integer ) msg.getObject( );
                  System.out.println( "[CLIENT]: Opponent amount: " + amount );
                  board.addOpponentHandCard( amount );

                }
                else if( msg.getObject( ) instanceof TurnData )
                {
                  TurnData data = ( TurnData ) msg.getObject( );
                  int i = 0;

                  while( !board.isVisible( ) )
                  {
                    try
                    {
                      System.out.println( ++i );
                      Thread.sleep( 100 );
                    }
                    catch( InterruptedException e )
                    {
                      // TODO Auto-generated catch block
                      e.printStackTrace( );
                    }
                  }

                  System.out.println( data.getPlayerID( ) + " --- " + player.getID( ) );
                  if( data.getPlayerID( ) == player.getID( ) )
                    board.initGame( false );
                  else
                    board.initGame( true );
                }
                else
                {

                }
              break;
              case REGISTER:

                final boolean b = ( Boolean ) msg.getObject( );

                if( !b )
                {
                  ( ( Register ) ScreenManager.getInstance( ).getScreen( ) ).getRegisterButton( )
                      .setTouchable( Touchable.enabled );
                  ;
                }

                ( ( Register ) ScreenManager.getInstance( ).getScreen( ) ).getRegisterText( ).setVisible( true );// new
                                                                                                                 // Label("danke
                                                                                                                 // f�r
                                                                                                                 // den
                                                                                                                 // schei�!",
                                                                                                                 // labelStyle);
                Timer.schedule( new Task( )
                {

                  @Override
                  public void run( )
                  {

                    System.out.println( "penis" );
                    if( b )
                    {

                      ScreenManager.getInstance( ).setScreen( new Login( ) );
                      System.out.println( "vagina" );

                    }

                  }
                }, 5 );

              break;

              case PLAY_CARD:

                if( msg.getObject( ) instanceof TurnData )
                {
                  TurnData data = ( TurnData ) msg.getObject( );
                  if( data.getPlayerID( ) == player.getID( ) )
                  {
                    System.out.println( "Player turn." );
                    if( data.getBoardIndex( ) >= 0 )
                    {
                      System.out.println( "Valid turn." );
                      validAction = true;
                      board.playCard( data );
                    }
                    else
                      System.out.println( "Invalid turn." );
                    validAction = false;
                  }
                  else
                  {
                    System.out.println( "Opponent turn." );
                    board.playOpponentCard( data );
                  }
                }
              break;
              case RESET_PW:
                if( msg.getObject( ) instanceof LoginData )
                {
                  LoginData reset = ( LoginData ) msg.getObject( );
                  System.out.println( "LoginData." );
                  try
                  {
                    Thread.sleep( 1000 );
                  }
                  catch( InterruptedException e )
                  {
                    e.printStackTrace( );
                  }
                  if( ScreenManager.getInstance( ).getScreen( ) instanceof Login )
                  {
                    System.out.println( "Login screen." );
                    Login screen = ( Login ) ScreenManager.getInstance( ).getScreen( );
                    screen.setUsernamePassword( reset );
                  }
                }
              break;
              default:
              break;
            }
          }
        }
      } );

    }
    catch( IOException e )
    {
      e.printStackTrace( );
    }
  }

  /**
   * method to get the {@link Player} object
   * 
   * @return {@link Player}
   */
  public Player getPlayer( )
  {
    return player;
  }

  /**
   * method to get the {@link User} object
   * 
   * @return {@link User}
   */
  public User getUser( )
  {
    return user;
  }

  /**
   * method to send the connect to room {@link Message}
   */
  public void connectToRoom( )
  {
    // Message msg = new Message( MessageType.CONNECT_TO_ROOM, getPlayer( ) );
    Message msg = new Message( MessageType.CONNECT_TO_ROOM, getUser( ) );

    client.sendTCP( msg );
  }

  /**
   * method to send the play card {@link Message}
   * 
   * @param data
   */
  public void sendPlayCard( TurnData data )
  {
    data.setPlayerID( player.getID( ) );
    data.setRoomID( player.getRoomID( ) );
    Message msg = new Message( MessageType.PLAY_CARD, data );

    client.sendTCP( msg );
  }

  /**
   * method to send the connect to server {@link Message}
   * 
   * @param login
   */
  public void connectToServer( LoginData login )
  {
    Message msg = new Message( MessageType.CONNECT_TO_SERVER, login );

    client.sendTCP( msg );
  }

  /**
   * method to send the end turn {@link Message}
   */
  public void endTurn( )
  {
    TurnData data = new TurnData( );
    data.setPlayerID( player.getID( ) );
    data.setRoomID( player.getRoomID( ) );
    Message msg = new Message( MessageType.END_TURN, data );

    client.sendTCP( msg );
  }

  /**
   * method to send the register {@link Message}
   * 
   * @param login
   */
  public void register( LoginData login )
  {
    Message msg = new Message( MessageType.REGISTER, login );
    client.sendTCP( msg );

  }

  /**
   * method to send the reset password {@link Message}
   * 
   * @param reset
   */
  public void resetPW( LoginData reset )
  {
    Message msg = new Message( MessageType.RESET_PW, reset );

    client.sendTCP( msg );
  }
  
  public void sendAttackMsg( )
  {
    Message msg = new Message( MessageType.ATTACK );
    AttackData data = new AttackData( player.getID( ), player.getRoomID( ), this.getOwnBoardIndex( ), this.getOpponentBoardIndex( ) );
    
    msg.setObject( data );
    
    client.sendTCP( msg );
    
    System.out.println( "Attack msg sended." );
  }

  /**
   * method to send the logout {@link Message} and remove the player out of the
   * room queue if necessary
   */
  public void removeUserFromQueue( )
  {
    Message msg = new Message( MessageType.LOGOUT, getUser( ) );
    client.sendTCP( msg );
  }

  /**
   * method to show the {@link Login} screen
   */
  public void logout( )
  {
    // client.stop( );
    // client.close( );
    ScreenManager.getInstance( ).setScreen( new Login( ) );
  }

  /**
   * method to get the validAction value
   * @return {@link Boolean}
   */
  public boolean isValidAction( )
  {
    return validAction;
  }

  /**
   * method to set the validAction value
   * @param validAction
   */
  public void setValidAction( boolean validAction )
  {
    this.validAction = validAction;
  }
  
  /**
   * method to set the own board index (attack)
   * @param index
   */
  public void setOwnBoardIndex( int index )
  {
    this.ownBoardIndex = index;
  }
  
  /**
   * method to get the own board index (attack)
   * @return {@link Integer}
   */
  public int getOwnBoardIndex( )
  {
    return ownBoardIndex;
  }
  
  /**
   * method to set the own board index (attack)
   * @param index
   */
  public void setOpponentBoardIndex( int index )
  {
    this.opponentBoardIndex = index;
    sendAttackMsg( );
  }
  
  /**
   * method to get the own board index (attack)
   * @return {@link Integer}
   */
  public int getOpponentBoardIndex( )
  {
    return opponentBoardIndex;
  }

  // public static void main( String[ ] args )
  // {
  //// Log.set( Log.LEVEL_DEBUG );
  // new KNClient( );
  // }

}
