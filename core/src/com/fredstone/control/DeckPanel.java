package com.fredstone.control;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.fredstone.model.Deck;
import com.fredstone.screens.BoardScreen;
import com.fredstone.screens.DeckSelection;
import com.fredstone.screens.MainMenu;
import com.fredstone.screens.ScreenManager;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class DeckPanel extends Table
{

  private String deckTitle;
  private Texture texture, textureWBorder;
  private Image img;
  private Image borderImage;
  private Label lblDeckTitle;
  private BitmapFont font;
  private LabelStyle labelStyle;
  private Deck playerDecK;

  public DeckPanel( Deck playerDeck )
  {
    this.playerDecK = playerDeck;

    final Deck playerdeck = playerDeck;

    this.deckTitle = playerDeck.getName( );
    texture = new Texture( Gdx.files.internal( "affe.jpg" ) );
    textureWBorder = new Texture( Gdx.files.internal( "affe_border.jpg" ) );
    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).setScale( 0.3f );
    labelStyle = new LabelStyle( font, Color.WHITE );
    img = new Image( texture );
    lblDeckTitle = new Label( this.deckTitle, labelStyle );

    add( img );
    row( );
    add( lblDeckTitle );
    // setDebug(true);
    // setFillParent(true);

    this.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent event, float x, float y )
      {

        System.out.println( playerdeck.getName( ) );
        // ScreenManager.getInstance().setScreen(new BoardScreen());
        // event.getRelatedActor().;
        // return playerDeck;
        // setImg(new Image(textureWBorder));
        // img.setDrawable(new TextureRegionDrawable(new
        // TextureRegion(textureWBorder)));

        // TODO: Starte die Suche nach einem Gegner mit dem aktuell
        // ausgewählten Deck

        // DeckSelection.ACTUAL_DECK = getPlayerDecK();

      }
    } );

  }

  /**
   * method to get the player deck
   * @return {@link Deck}
   */
  public Deck getPlayerDecK( )
  {
    return playerDecK;
  }

  /**
   * method to set the player deck
   * @param playerDecK
   */
  public void setPlayerDecK( Deck playerDecK )
  {
    this.playerDecK = playerDecK;
  }

  /**
   * method to get the deck title
   * @return {@link String}
   */
  public String getDeckTitle( )
  {
    return deckTitle;
  }

  /**
   * method to set the deck title
   * @param deckTitle
   */
  public void setDeckTitle( String deckTitle )
  {
    this.deckTitle = deckTitle;
  }

  /**
   * method to get the deck texture
   * @return {@link Texture}
   */
  public Texture getTexture( )
  {
    return texture;
  }

  /**
   * method to set the deck texture
   * @param texture
   */
  public void setTexture( Texture texture )
  {
    this.texture = texture;
  }

  /**
   * method to get the deck image
   * @return {@link Image}
   */
  public Image getImg( )
  {
    return img;
  }

  /**
   * method to set the deck image
   * @param img
   */
  public void setImg( Image img )
  {
    this.img = img;
  }

  /**
   * method to get the border image
   * @return {@link Image}
   */
  public Image getBorderImage( )
  {
    return borderImage;
  }

  /**
   * method to set the border image
   * @param borderImage
   */
  public void setBorderImage( Image borderImage )
  {
    this.borderImage = borderImage;
  }

  /**
   * method to get the deck title label
   * @return {@link Label}
   */
  public Label getLblDeckTitle( )
  {
    return lblDeckTitle;
  }

  /**
   * method to set the deck title label
   * @param lblDeckTitle
   */
  public void setLblDeckTitle( Label lblDeckTitle )
  {
    this.lblDeckTitle = lblDeckTitle;
  }

  /**
   * method to get the font
   * @return {@link BitmapFont}
   */
  public BitmapFont getFont( )
  {
    return font;
  }

  /**
   * method to set the font
   * @param font
   */
  public void setFont( BitmapFont font )
  {
    this.font = font;
  }

  /**
   * method to get the label style
   * @return {@link LabelStyle}
   */
  public LabelStyle getLabelStyle( )
  {
    return labelStyle;
  }

  /**
   * method to set the label style
   * @param labelStyle
   */
  public void setLabelStyle( LabelStyle labelStyle )
  {
    this.labelStyle = labelStyle;
  }

}
