package com.fredstone.control;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class MenuLabel
{

  private BitmapFont font;
  private LabelStyle labelStyle;

  private Label label;

  public MenuLabel( String text )
  {

    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).setScale( 0.3f );
    labelStyle = new LabelStyle( font, Color.WHITE );

    label = new Label( text, labelStyle );
  }

  /**
   * method to get the label
   * @return {@link Label}
   */
  public Label getLabel( )
  {
    return label;
  }

  /**
   * method to set the label
   * @param label
   */
  public void setLabel( Label label )
  {
    this.label = label;
  }

  /**
   * method to get the font
   * @return {@link BitmapFont}
   */
  public BitmapFont getFont( )
  {
    return font;
  }

  /**
   * method to set the font
   * @param font
   */
  public void setFont( BitmapFont font )
  {
    this.font = font;
  }

  /**
   * method to get the label style
   * @return {@link LabelStyle}
   */
  public LabelStyle getLabelStyle( )
  {
    return labelStyle;
  }

  /**
   * method to set the label style
   * @param labelStyle
   */
  public void setLabelStyle( LabelStyle labelStyle )
  {
    this.labelStyle = labelStyle;
  }

}
