package com.fredstone.screens;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.fredstone.FredStoneGame;
import com.fredstone.actor.CardActor;
import com.fredstone.actor.CardActorGroup;
import com.fredstone.model.AttackData;
import com.fredstone.model.Card;
import com.fredstone.model.Player;
import com.fredstone.model.TurnData;
import com.fredstone.utils.CardList;

public class BoardScreen extends ScreenAdapter implements ScreenOfCardgroup
{
  public static final int BOARDWIDTH = 400, BOARDHEIGHT = 230;
  // public static final int BOARDWIDTH = 1920, BOARDHEIGHT = 1080;

  private Stage stage;
  private Set<CardActorGroup> cardGroups = new HashSet<CardActorGroup>( );
  private ArrayList<CardActor> testcards;
  private Player player, opponent;
  private CardActorGroup hand, mySide, opponentHand, opponentSide;
  private Sprite background;

  private TextureAtlas atlas;
  private Skin skin;

  private TextButton endTurnButton;

  private Label playerNameLbl, playerHPLbl, playerMCLabel, opponentNameLbl, opponentHPLabel, opponentMCLabel,
      statusLabel;
  private BitmapFont font;
  private LabelStyle labelStyle;
  private String statusText;
  private Touchable endTurnButtonTouchable;
  private boolean active, visible = false;

  private final String ACTIVE_TXT = "Your turn", INACTIVE_TXT = "Opponents turn";
  private boolean validAction;

  public BoardScreen( Player player, Player opponent )
  {

    this.player = player;
    this.opponent = opponent;

    hand = new CardActorGroup( 0, 0, BOARDWIDTH, BOARDHEIGHT / 4, "hand", true );
    mySide = new CardActorGroup( 75, hand.getHEIGHT( ), BOARDWIDTH - 150, BOARDHEIGHT / 4, "board", true );
    opponentHand = new CardActorGroup( 0, BOARDHEIGHT - hand.getHEIGHT( ), BOARDWIDTH, BOARDHEIGHT / 4, "hand", false );
    opponentSide = new CardActorGroup( 75, BOARDHEIGHT - hand.getHEIGHT( ) - hand.getHEIGHT( ), BOARDWIDTH - 150,
        BOARDHEIGHT / 4, "board", false );

  }

  @Override
  public void resize( int width, int height )
  {
    stage.getViewport( ).update( width, height, true );
  }

  @Override
  public void show( )
  {
    stage = new Stage( new ScalingViewport( Scaling.stretch, BOARDWIDTH, BOARDHEIGHT, new OrthographicCamera( ) ) );
    Gdx.input.setInputProcessor( stage );
    background = new Sprite( new Texture( Gdx.files.internal( "HSBoard.png" ) ) );
    // background.setBounds(background.getX(), background.getY(), BOARDWIDTH,
    // BOARDHEIGHT);
    // background.setWrap(Texture.TextureWrap.ClampToEdge,
    // Texture.TextureWrap.ClampToEdge);

    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );

    skin = new Skin( atlas );

    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );

    font.getData( ).setScale( 0.07f );
    labelStyle = new LabelStyle( font, Color.WHITE );
    // TODO Auto-generated constructor stub

    playerNameLbl = new Label( player.getName( ), labelStyle );
    playerMCLabel = new Label( "Mana: " + player.getManaCrystals( ), labelStyle );
    System.out.println( "[BoardScreen]: Player: " + player.getName( ) + " , Mana: " + player.getManaCrystals( ) );
    opponentNameLbl = new Label( opponent.getName( ), labelStyle );
    opponentMCLabel = new Label( "Mana: " + opponent.getManaCrystals( ), labelStyle );
    System.out.println( "[BoardScreen]: Opponent: " + opponent.getName( ) + " , Mana: " + opponent.getManaCrystals( ) );

    TextButtonStyle buttonStyle = new TextButtonStyle( );
    buttonStyle.up = skin.getDrawable( "btn_up" );
    buttonStyle.down = skin.getDrawable( "btn_down" );
    buttonStyle.pressedOffsetX = 1;
    buttonStyle.pressedOffsetY = -1;
    buttonStyle.font = font;
    buttonStyle.fontColor = Color.BLACK;
    buttonStyle.downFontColor = Color.WHITE;

    endTurnButton = new TextButton( "End Turn", buttonStyle );
    endTurnButton.setPosition( BOARDWIDTH - endTurnButton.getWidth( ),
        BOARDHEIGHT / 2 - endTurnButton.getHeight( ) / 2 );
    endTurnButton.setTouchable( endTurnButtonTouchable );
    endTurnButton.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        FredStoneGame.client.endTurn( );
      }
    } );

    stage.addActor( endTurnButton );

    statusLabel = new Label( statusText, labelStyle );
    statusLabel.setPosition( BOARDWIDTH / 8 - statusLabel.getWidth( ) / 2, BOARDHEIGHT - statusLabel.getHeight( ) * 4 );
    stage.addActor( statusLabel );

    playerNameLbl.setPosition( BOARDWIDTH - playerNameLbl.getWidth( ), 0 + playerNameLbl.getHeight( ) );
    playerMCLabel.setPosition( BOARDWIDTH - playerNameLbl.getWidth( ), 0 + ( playerNameLbl.getHeight( ) * 2 ) );

    opponentNameLbl.setPosition( BOARDWIDTH - opponentNameLbl.getWidth( ), BOARDHEIGHT - opponentNameLbl.getHeight( ) );
    opponentMCLabel.setPosition( BOARDWIDTH - opponentNameLbl.getWidth( ),
        BOARDHEIGHT - ( opponentNameLbl.getHeight( ) * 2 ) );

    stage.addActor( playerNameLbl );
    stage.addActor( opponentNameLbl );

    cardGroups.add( hand );

    cardGroups.add( mySide );

    cardGroups.add( opponentHand );

    cardGroups.add( opponentSide );

    // testcards = new ArrayList<CardActor>();
    // for ( int i = 0; i < 7; i++) {
    // Card card = new Card();
    // card.setCardType(CardType.MONSTER);
    // CardActor testCardActor = new CardActor(this, new
    // Texture(Gdx.files.internal("blankhs.png")), card);
    // stage.addActor(testCardActor);
    // hand.add(testCardActor, 0);
    // }
    visible = true;
  }

  @Override
  public void render( float delta )
  {

    Gdx.gl.glClearColor( 0, 0, 0, 1 );
    Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
    stage.act( delta );
    stage.getBatch( ).begin( );
    // stage.getBatch( ).draw( background, 0, 0 );
    stage.getBatch( ).draw( background, 0, 0, BOARDWIDTH, BOARDHEIGHT );
    stage.getBatch( ).end( );
    stage.draw( );
  }

  @Override
  public void dispose( )
  {
    for( Actor a : stage.getActors( ) )
    {
      if( a instanceof CardActor )
      {
        ( ( CardActor ) a ).dispose( );
      }
    }
    stage.dispose( );
    stage.dispose( );
  }

  /**
   * method to check which {@link CardActorGroup} contains the positions
   * @param x
   * @param y
   * @return {@link CardActorGroup}
   */
  public CardActorGroup groupContainsPoint( float x, float y )
  {
    for( CardActorGroup group : cardGroups )
    {
      if( group.getRect( ).contains( x, y ) )
      {
        return group;
      }
    }
    return null;
  }
  
  public void updateBoardCards( AttackData data )
  {
    final CardActor playerActor = mySide.getCardActorByIndex( data.getOwnIndex( ) );
    final CardActor oppActor = opponentSide.getCardActorByIndex( data.getOpponentIndex( ) );
    Card playerCard = playerActor.getCard( );
    Card oppCard = oppActor.getCard( );
    
    System.out.println( "Player card defense: " + data.getPlayerCardDefense( ) );
    playerCard.setDefense( data.getPlayerCardDefense( ) );
    System.out.println( "Opponent card defense: " + data.getOpponentCardDefense( ) );
    oppCard.setDefense( data.getOpponentCardDefense( ) );
    
    if( playerCard.getDefense( ) <= 0 )
    {
      mySide.remove( playerActor );
      Gdx.app.postRunnable( new Runnable( ) {

        @Override
        public void run( )
        {
          playerActor.moveTo( -100, -100 );
        }
        
      });
    }
    
    if( oppCard.getDefense( ) <= 0 )
    {
      opponentSide.remove( oppActor );
      Gdx.app.postRunnable( new Runnable( ) {

        @Override
        public void run( )
        {
          oppActor.moveTo( -100, -100 );
        }
        
      });
    }
  }

  /**
   * method to add a {@link Card} to the player hand
   * @param card
   */
  public void addHandCard( final Card card )
  {
    final BoardScreen board = this;
    Gdx.app.postRunnable( new Runnable( )
    {

      @Override
      public void run( )
      {
        CardActor actor = new CardActor( board, card, hand.getTouchable( ) );
        stage.addActor( actor );
        hand.add( actor, hand.getListSize( ) );
      }

    } );
  }

  /**
   * method to add a {@link Card} to the opponents hand
   * @param amount
   */
  public void addOpponentHandCard( final int amount )
  {
    final BoardScreen board = this;

    Gdx.app.postRunnable( new Runnable( )
    {

      @Override
      public void run( )
      {
        for( int i = 0; i < amount; i++ )
        {
          CardActor actor = new CardActor( board, null, opponentHand.getTouchable( ) );
          // actor.setSprite( new Sprite( new Texture( Gdx.files.internal(
          // "cardback.png" ) ) ) );
          stage.addActor( actor );
          stage.cancelTouchFocus( actor );
          opponentHand.add( actor, 0 );
        }
      }

    } );
  }

  /**
   * method to move a {@link Card} from the player hand {@link CardActorGroup} to the board group 
   * @param data
   */
  public void playCard( final TurnData data )
  {
    final BoardScreen board = this;

    Gdx.app.postRunnable( new Runnable( )
    {

      @Override
      public void run( )
      {
        CardActor actor = hand.getCardActorByIndex( data.getHandIndex( ) );
        hand.remove( actor );
        mySide.add( actor, data.getBoardIndex( ) );
      }

    } );
  }

  /**
   * method to move a {@link Card} from the opponent hand {@link CardActorGroup} to the board group 
   * @param data
   */
  public void playOpponentCard( final TurnData data )
  {
    final BoardScreen board = this;

    Gdx.app.postRunnable( new Runnable( )
    {

      @Override
      public void run( )
      {
        // CardActor actor = new CardActor( board, CardList.getInstance(
        // ).getCard( data.getCardID( ) ) );
        // stage.addActor( actor );
        System.out.println( "Hand size: " + opponentHand.getListSize( ) );
        System.out.println( "Hand index: " + data.getHandIndex( ) );
        int index = opponentHand.getListSize( ) - data.getHandIndex( );
        if( index < 0 )
          index = 0;
        else if( index == opponentHand.getListSize( ) )
          index -= 1;

        System.out.println( "Index: " + index );

        CardActor actor = opponentHand.getCardActorByIndex( index );
        opponentHand.remove( actor );
        actor.setCard( Card.copy( CardList.getInstance( ).getCard( data.getCardID( ) ) ) );

        index = opponentSide.getListSize( ) - data.getBoardIndex( );
        if( index < 0 )
          index = 0;
        // System.out.println( "List size: " + opponentSide.getListSize( ) );
        // System.out.println( "Board index: " + data.getBoardIndex( ) );
        // System.out.println( "Index: " + index );

        opponentSide.add( actor, index );
      }

    } );
  }

  /**
   * method to check if the {@link BoardScreen} is visible
   * @return
   */
  public boolean isVisible( )
  {
    return visible;
  }

  /**
   * method to get the player hand
   * @return {@link CardActorGroup}
   */
  public CardActorGroup getHand( )
  {
    return hand;
  }

  /**
   * method to get the opponent hand
   * @return {@link CardActorGroup}
   */
  public CardActorGroup getOpponentHand( )
  {
    return opponentHand;
  }

  /**
   * method to toggle the touchable value for the players {@link CardActorGroup}
   */
  public void toggleTouchableActGrps( )
  {
    Iterator<CardActorGroup> it = cardGroups.iterator( );
    CardActorGroup cag;

    while( it.hasNext( ) )
    {
      cag = it.next( );
      if( cag.isOwnGroup( ) )
      {
        cag.toggleTouchable( );
      }
    }
  }

  /**
   * method to set the touchable value for the players {@link CardActorGroup}
   * @param touchable
   */
  public void setTouchableActGrps( boolean touchable )
  {
    Iterator<CardActorGroup> it = cardGroups.iterator( );
    CardActorGroup cag;
    System.out.println( "BoardScreen: CardGoups size " + cardGroups.size( ) );

    while( it.hasNext( ) )
    {
      cag = it.next( );
      if( cag.isOwnGroup( ) )
      {
        System.out.println( "setTouchableActGrps: " + touchable );
        cag.setTouchable( touchable );
      }
    }
  }

  /**
   * method to check if the {@link BoardScreen} is active
   * @return
   */
  public boolean isActive( )
  {
    return active;
  }

  /**
   * method to initialize the {@link BoardScreen}
   * @param active
   */
  public void initGame( boolean active )
  {
    this.active = active;

    System.out.println( "initGame: " + active );

    setLabelButton( );

    setTouchableActGrps( active );
  }

  /**
   * method to toggle the active value and players {@link CardActorGroup}
   */
  public void toggleActive( )
  {
    this.active = !this.active;

    setLabelButton( );

    toggleTouchableActGrps( );
  }

  /**
   * method to set the label text and enable/disable the end turn button
   */
  private void setLabelButton( )
  {
    if( active )
    {
      setStatusLabelText( ACTIVE_TXT );
      // endTurnButton.setTouchable( Touchable.enabled );
      endTurnButtonTouchable = Touchable.enabled;
    }
    else
    {
      setStatusLabelText( INACTIVE_TXT );
      // endTurnButton.setTouchable( Touchable.disabled );
      endTurnButtonTouchable = Touchable.disabled;
    }

    if( endTurnButton != null )
      endTurnButton.setTouchable( endTurnButtonTouchable );
  }

  /**
   * method to set the status label text
   * @param text
   */
  public void setStatusLabelText( String text )
  {
    this.statusText = text;

    if( this.statusLabel != null )
      this.statusLabel.setText( text );
  }

  /**
   * method to get the valid action value
   * @return {@link Boolean}
   */
  public boolean isValidAction( )
  {
    return validAction;
  }

  /**
   * method to set the valid action value
   * @param validAction
   */
  public void setValidAction( boolean validAction )
  {
    this.validAction = validAction;
  }

}
