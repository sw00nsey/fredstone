package com.fredstone.screens;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.fredstone.actor.CardActor;
import com.fredstone.actor.CardActorForCardCollection;
import com.fredstone.actor.CardActorGroup;
import com.fredstone.model.Card;
import com.fredstone.model.Deck;
import com.fredstone.model.User;
import com.fredstone.utils.CardList;
import com.fredstone.utils.CardType;

public class CardCollection implements Screen, ScreenOfCardgroup {

	private ArrayList<Card> listOfCards = CardList.getInstance().getCardCollection();
	private ArrayList<Deck> decklist;
	private ArrayList<CardActor> cardActors = new ArrayList<CardActor>();;

	private Set<CardActorGroup> cardGroups = new HashSet<CardActorGroup>();
	private ArrayList<Card> newCardsOfDeck = new ArrayList<Card>();
	private Skin skin;
	private User user;
	private Stage stage;
	private BitmapFont font;
	private TextureAtlas atlas;
	private Table cardTable, deckTable;
	private TextButton backButton, nextPage, previousPage;
	private int startIndex = 0;
	private Label cardCounter;
	private LabelStyle labelStyle;
	private Deck currentDeck;
	private Label isInDeck;
	private static final int HEIGHT = 230;
	private static final int WIDTH = 400;
	
	private Sprite sprite = new Sprite(new Texture(Gdx.files.internal("blankhs.png")));
	
	public CardCollection(User user) {
		this.user = user;
		this.decklist = user.getDeckList();
	}

	@Override
	public void show() {
		
		for (Card card : listOfCards) { // test!
			card.setCardType(CardType.MONSTER);
		}
		
		stage = new Stage(new ScalingViewport(Scaling.stretch, WIDTH, HEIGHT, new OrthographicCamera()));
		Gdx.input.setInputProcessor( stage );
		atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );
	    skin = new Skin( atlas );	    
	    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
	    font.getData( ).setScale( 0.07f );
	    labelStyle = new LabelStyle( font, Color.WHITE );
	    
	    cardTable = new Table();
	    cardTable.debug();
	    cardTable.setSize(WIDTH*3/4, HEIGHT );
	    cardTable.setPosition(0, 0 );
	    
	    deckTable = new Table();
	   
	    deckTable.debug();
	    deckTable.setSize(WIDTH/4 , HEIGHT);
	    deckTable.setPosition( ( WIDTH - WIDTH/4)  ,0);
	    
	    
	    
	    
	    Iterator<Deck> deckIterator = decklist.iterator();
	    
	    while (deckIterator.hasNext()) {
	    	Deck deckTemp	= deckIterator.next();
	    	for(int i=0; i < deckTemp.getCards().size(); i++) {
	    	//	listOfCards.add(deckTemp.getCards().get(i));
	    	}
		}
	    
	    isInDeck = new Label("Is in Deck", labelStyle);
	    cardCounter.setX(WIDTH*3/4);
	    cardCounter.setY(HEIGHT/2);
	    /*
	     * create arrayList of cardActor
	     */
	    CardActor testCard;
	    for(int i =0; i < 8; ++i) {
	    	testCard = new CardActorForCardCollection(this, null, true);
	    	createClickListener(testCard);
		    testCard.setSize(40, 54.3f);
		    cardActors.add(testCard);
	    	//cardActors.get(i).setTouchable(Touchable.disabled);
	    	cardTable.add(testCard);
	    	if(i  == 3) {
				cardTable.row( );
			}
	    	
	    }
	    
	    TextButtonStyle buttonStyle = new TextButtonStyle( );
	    buttonStyle.up = skin.getDrawable( "btn_up" );
	    buttonStyle.down = skin.getDrawable( "btn_down" );
	    buttonStyle.pressedOffsetX = 1;
	    buttonStyle.pressedOffsetY = -1;
	    buttonStyle.font = font;
	    buttonStyle.fontColor = Color.BLACK;
	    buttonStyle.downFontColor = Color.WHITE;
	    
	    int counter = 0;
	    List<TextButton> deckbuttons = new ArrayList<TextButton>();
	    
	    
	    for ( final Deck deck : decklist) {
	    	System.out.println(deck.getName());
	    	TextButton temp = new TextButton(deck.getName(), buttonStyle);
	    	temp.setPosition(20,50+counter);
	    	temp.addListener(new ClickListener(){
		    	 @Override
		         public void clicked( InputEvent evt, float x, float y )
		         {
		    		 currentDeck = deck;
		    		 cardCounter.setText( deck.getCards().size() + "/30");
		    		 System.out.println(deck.getCards());
		    		 startIndex = 0;
		    		 setCardgroup(currentDeck.getCards());
		         }
		    });
	    	counter +=20;
	    	deckbuttons.add(temp);
	    }
    	
    	for(TextButton deckbutton : deckbuttons) {
    		deckTable.addActor(deckbutton);
    	}

	    
		backButton = new TextButton("Back", buttonStyle);
	    backButton.addListener(new ClickListener(){
	    	 @Override
	         public void clicked( InputEvent evt, float x, float y )
	         {
	    		  ScreenManager.getInstance().setScreen(new MainMenu());
	         }
	    });
	    
	    nextPage = new TextButton("next Page", buttonStyle);
	    nextPage.setPosition(200,0);
	    nextPage.addListener(new ClickListener(){
	    	 @Override
	         public void clicked( InputEvent evt, float x, float y )
	         {
    			 startIndex+=8;
	    		 setCardgroup(currentDeck != null ? currentDeck.getCards() : listOfCards);
	         }
	         
	    });
	    
	    previousPage = new TextButton("previous Page", buttonStyle);
	    previousPage.setPosition(0,0);
	    previousPage.addListener(new ClickListener(){
	    	 @Override
	         public void clicked( InputEvent evt, float x, float y )
	         {
	    		 if(startIndex > 7) {
	    			 startIndex -= 8;
		    		 setCardgroup(currentDeck != null ? currentDeck.getCards() : listOfCards);
	    		 }
	         }
	    });	    
	    System.out.println(currentDeck);
	    cardCounter = new Label("0/30", labelStyle);
	    
	    cardCounter.setX(WIDTH*3/4);
	    cardCounter.setY(HEIGHT/2);
	    setCardgroup(listOfCards);
	    cardTable.row();
	    cardTable.addActor(previousPage);
	    cardTable.row();
	    cardTable.addActor(nextPage);
	    stage.addActor(cardCounter);
	   
		
		stage.addActor(cardTable);
		stage.addActor(deckTable);
		
		deckTable.addActor(backButton);
	}

	private void setCardgroup(ArrayList<Card> tempCardList) {
		for(CardActor a : cardActors) {
			a.setCard(null);
			a.setSprite(null);
		}
		CardActor cardActor;
		for(int i=0; i<8; ++i ) {
			System.out.println("i: "+i);
			cardActor = cardActors.get(i);
			
			int deckIndex = startIndex+i;
			System.out.println("i: "+i+"; deckIndex: "+deckIndex+"; tempCardList.size(): "+tempCardList.size());
			if (deckIndex < tempCardList.size()) {
				System.out.println("Type: " + tempCardList.get(deckIndex).getType());
				cardActor.setCard(tempCardList.get(deckIndex));
				cardActor.setSprite(sprite);
			} else {
//				cardActor.setCard(null);
//				cardActor.setSprite(null);
			}
		}
	}
	private void createClickListener( final CardActor testCard) {
		
		testCard.addListener(new ClickListener(){
			
	    	 @Override
	         public void clicked( InputEvent evt, float x, float y )
	         {
	    		 System.out.println("click!");
	    		 putCardInListOfCardsForDeck(testCard);
	         }
	         
	    });
		
    }
	private void putCardInListOfCardsForDeck(CardActor testCard) {
		boolean isNotReady = true;
		if (isNotReady) {
			if (newCardsOfDeck.contains(testCard.getCard())) {
				newCardsOfDeck.remove(testCard.getCard());
				isNotReady = false;
			} else {
				newCardsOfDeck.add(testCard.getCard());
				isNotReady = false;
			}
		System.out.println(newCardsOfDeck);
		}
		
	}
	public CardActorGroup groupContainsPoint(float x, float y) {
		for (CardActorGroup group : cardGroups) {
			if (group.getRect().contains(x, y)) {
				return group;
			}
		}
		return null;
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
