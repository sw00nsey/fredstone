package com.fredstone.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.fredstone.FredStoneGame;

public class ConnectToRoom implements Screen
{
  private Stage stage;

  private Table table;
  private Label textLabel, dotLabel;
  private LabelStyle labelStyle;
  private TextureAtlas atlas;
  private Skin skin;
  private TextButton cancelBtn;
  private TextButtonStyle btnStyle;
  private BitmapFont font;
  private static final int HEIGHT = 900;
  private static final int WIDTH = 1600;

  private Timer timer;

  private String text, dots;

  @Override
  public void show( )
  {
    stage = new Stage( new ScalingViewport( Scaling.stretch, WIDTH, HEIGHT, new OrthographicCamera( ) ) );
    Gdx.input.setInputProcessor( stage );

    text = "Connecting";
    dots = ".";

    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );
    skin = new Skin( atlas );

    table = new Table( );

    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).scale( 0.05f );
    labelStyle = new LabelStyle( font, Color.WHITE );

    textLabel = new Label( text, labelStyle );

    dotLabel = new Label( dots, labelStyle );

    font.getData( ).setScale( 0.4f );

    btnStyle = new TextButtonStyle( );
    btnStyle.up = skin.getDrawable( "btn_up" );
    btnStyle.down = skin.getDrawable( "btn_down" );
    btnStyle.pressedOffsetX = 1;
    btnStyle.pressedOffsetY = -1;
    btnStyle.font = font;
    btnStyle.fontColor = Color.BLACK;
    btnStyle.downFontColor = Color.WHITE;

    cancelBtn = new TextButton( "CANCEL", btnStyle );
    // cancelBtn.setSize( 50, 30 );
    int width = Gdx.graphics.getWidth( );
    cancelBtn.setPosition( width / 2 - ( cancelBtn.getWidth( ) / 2 ), 25 );

    cancelBtn.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new MainMenu( ) );
        FredStoneGame.client.removeUserFromQueue( );
      }
    } );

    table.setFillParent( true );
    table.add( textLabel );
    table.add( dotLabel );

    stage.addActor( table );
    stage.addActor( cancelBtn );

    timer = new Timer( );
    timer.scheduleTask( new Task( )
    {

      @Override
      public void run( )
      {
        if( dots.length( ) < 4 )
          dots += ".";
        else
          dots = ".";

        dotLabel.setText( dots );
      }

    }, 0.25f, 0.5f );

    timer.start( );
  }

  @Override
  public void render( float delta )
  {
    Gdx.gl.glClearColor( 0, 0, 0, 1 );
    Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

    stage.act( delta );
    stage.getBatch( ).begin( );
    stage.getBatch( ).end( );

    stage.draw( );
  }

  @Override
  public void resize( int width, int height )
  {

  }

  @Override
  public void pause( )
  {

  }

  @Override
  public void resume( )
  {

  }

  @Override
  public void hide( )
  {

  }

  @Override
  public void dispose( )
  {
    stage.dispose( );
    font.dispose( );

    timer.stop( );
  }

}
