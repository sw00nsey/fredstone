package com.fredstone.screens;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fredstone.FredStoneGame;
import com.fredstone.control.DeckPanel;
import com.fredstone.control.MenuLabel;
import com.fredstone.model.Deck;
import com.fredstone.model.Player;
import com.fredstone.model.User;

public class DeckSelection implements Screen
{
  private User user;
  private Stage stage;
  private TextureAtlas atlas;

  private static final int HEIGHT = 900;
  private static final int WIDTH = 1600;
  // private static final int HEIGHT = 450;
  // private static final int WIDTH = 800;
  private Table deckTable;

  private TextButton backButton;
  private Skin skin;
  private BitmapFont font;
  private ArrayList<Deck> deckList;

  // public static Deck ACTUAL_DECK = null;
  // private Deck actualDeck;

  public DeckSelection( User user )
  {
    // TODO Auto-generated constructor stub
    this.user = user;
    this.deckList = user.getDeckList( );
  }

  @Override
  public void show( )
  {

    stage = new Stage( new ScalingViewport( Scaling.stretch, WIDTH, HEIGHT, new OrthographicCamera( ) ) );
    Gdx.input.setInputProcessor( stage );
    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );

    MenuLabel a = new MenuLabel( "Deckselection" );
    a.getLabel( ).setPosition( WIDTH / 2 - ( a.getLabel( ).getWidth( ) / 2 ),
        HEIGHT - ( a.getLabel( ).getHeight( ) * 2 ) );

    // Deck testDeck = new Deck();
    // testDeck.setName("Innkeeper");
    //
    // DeckPanel pnlDeck = new DeckPanel(testDeck);
    // DeckPanel b = new DeckPanel(testDeck);
    // DeckPanel c = new DeckPanel(testDeck);

    deckTable = new Table( );
    // deckTable.setFillParent(true);
    deckTable.setSize( WIDTH / 2, HEIGHT / 2 );

    deckTable.setDebug( true );

    // deckTable.add(pnlDeck).pad(20);
    // deckTable.add(b).pad(20);
    // deckTable.add(c).pad(20);

    Iterator<Deck> deckIterator = deckList.iterator( );
    while( deckIterator.hasNext( ) )
    {
      DeckPanel temp = new DeckPanel( deckIterator.next( ) );
      deckTable.add( temp ).pad( 20 );
    }

    deckTable.setPosition( ( WIDTH / 4 ), HEIGHT / 3 );
    // deckTable.align(a)

    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );
    skin = new Skin( atlas );
    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).setScale( 0.3f );

    TextButtonStyle buttonStyle = new TextButtonStyle( );
    buttonStyle.up = skin.getDrawable( "btn_up" );
    buttonStyle.down = skin.getDrawable( "btn_down" );
    buttonStyle.pressedOffsetX = 1;
    buttonStyle.pressedOffsetY = -1;
    buttonStyle.font = font;
    buttonStyle.fontColor = Color.BLACK;
    buttonStyle.downFontColor = Color.WHITE;

    backButton = new TextButton( "Back", buttonStyle );
    backButton.setPosition( ( WIDTH / 2 ) - ( backButton.getWidth( ) / 2 ), HEIGHT / 5 );

    backButton.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new MainMenu( ) );
      }
    } );

    stage.addActor( backButton );
    stage.addActor( a.getLabel( ) );
    stage.addActor( deckTable );

    System.out.println( "B: " + deckTable.getWidth( ) + " und H: " + deckTable.getHeight( ) );

  }

  @Override
  public void render( float delta )
  {
    // TODO Auto-generated method stub
    Gdx.gl.glClearColor( 0, 0, 0, 1 );
    Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

    stage.act( delta );

    stage.draw( );

  }

  @Override
  public void resize( int width, int height )
  {
    // TODO Auto-generated method stub
    stage.getViewport( ).update( width, height );

  }

  @Override
  public void pause( )
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void resume( )
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void hide( )
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void dispose( )
  {
    // TODO Auto-generated method stub
    stage.dispose( );
  }

}
