package com.fredstone.screens;

import java.beans.FeatureDescriptor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fredstone.FredStoneGame;
import com.fredstone.model.LoginData;
import com.fredstone.utils.Message;

public class Login implements Screen
{
  private final int width = 800;
  private final int height = 475;
  private Stage stage;
  private TextureAtlas atlas;
  private Skin skin;
  private BitmapFont font;
  private Table table;
  private Label usernameLabel;
  private Label passwordLabel;
  private TextField usernameField;
  private TextField passwordField;
  private TextButton loginButton;
  private TextButton RegisterButton;
  private TextButton resetPW;
  private Texture backgroundTexture, fredStoneLogoTexture, fredStoneLoginBoard;
  private Label infoLabel;
  // private TextureRegion backgroundRegion;

  @Override
  public void show( )
  {
    Gdx.graphics.setWindowedMode( width, height );

    stage = new Stage( );
    table = new Table( );
    Gdx.input.setInputProcessor( stage );
    backgroundTexture = new Texture( Gdx.files.internal( "login.png" ) );
    fredStoneLogoTexture = new Texture( Gdx.files.internal( "FSLogo.png" ) );
    fredStoneLoginBoard = new Texture( Gdx.files.internal( "loginBoard.png" ) );

    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );
    skin = new Skin( atlas );
    Skin testSkin = new Skin( Gdx.files.internal( "uiskin.json" ) );

    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).setScale( 0.3f );

    LabelStyle labelStyle = new LabelStyle( font, Color.WHITE );
    usernameLabel = new Label( "Username:", labelStyle );
    passwordLabel = new Label( "Password:", labelStyle );

    infoLabel = new Label( "", labelStyle );
    infoLabel.setPosition( width / 2 - infoLabel.getWidth( ) / 2, height / 2 - infoLabel.getHeight( ) / 2 );
    infoLabel.setVisible( false );

    TextFieldStyle fieldStyle = new TextFieldStyle( font, Color.WHITE, testSkin.getDrawable( "cursor" ), null, null );

    usernameField = new TextField( "", fieldStyle );
    usernameField.setTextFieldListener( getEnterListener( ) );

    passwordField = new TextField( "", fieldStyle );
    passwordField.setTextFieldListener( getEnterListener( ) );
    passwordField.setPasswordCharacter( '*' );
    passwordField.setPasswordMode( true );

    TextButtonStyle buttonStyle = new TextButtonStyle( );
    buttonStyle.up = skin.getDrawable( "btn_up" );
    buttonStyle.down = skin.getDrawable( "btn_down" );
    buttonStyle.pressedOffsetX = 1;
    buttonStyle.pressedOffsetY = -1;
    buttonStyle.font = font;
    buttonStyle.fontColor = Color.BLACK;
    buttonStyle.downFontColor = Color.WHITE;

    loginButton = new TextButton( "LOGIN", buttonStyle );
    loginButton.addListener( new ClickListener( )
    {

      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        connectToServer( );
      }
    } );

    RegisterButton = new TextButton( "REGISTER", buttonStyle );
    RegisterButton.addListener( new ClickListener( )
    {

      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new Register( ) );
      }
    } );

    resetPW = new TextButton( "RESET PW", buttonStyle );
    resetPW.addListener( new ClickListener( )
    {

      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new ResetPassword( ) );
      }
    } );

    table.add( usernameLabel );
    table.add( usernameField );

    table.row( );

    table.add( passwordLabel );
    table.add( passwordField );

    table.row( );

    table.add( loginButton );
    table.add( RegisterButton );
    table.add( resetPW );

    // table.setFillParent( true );

    table.setDebug( true );
    table.debugTable( );
    // table.setPosition( WIDTH - table.getWidth(), HEIGHT - table.getHeight());
    table.setPosition( ( width / 2 ) - ( table.getWidth( ) / 2 ), 100 );

    stage.addActor( table );
    stage.addActor( infoLabel );
    stage.setKeyboardFocus( usernameField );
  }

  @Override
  public void render( float delta )
  {
    Gdx.gl.glClearColor( 0, 0, 0, 1 );
    Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

    stage.act( delta );
    stage.getBatch( ).begin( );
    stage.getBatch( ).draw( backgroundTexture, 0, 0 );
    stage.getBatch( ).draw( fredStoneLogoTexture, ( width / 2 ) - fredStoneLogoTexture.getWidth( ) / 2,
        table.getHeight( ) + 100 );
    stage.getBatch( ).draw( fredStoneLoginBoard, ( width / 2 ) - fredStoneLoginBoard.getWidth( ) / 2,
        table.getHeight( ) + 25 );
    stage.getBatch( ).end( );

    stage.draw( );
  }

  @Override
  public void resize( int width, int height )
  {

  }

  @Override
  public void pause( )
  {

  }

  @Override
  public void resume( )
  {

  }

  @Override
  public void hide( )
  {

  }

  @Override
  public void dispose( )
  {
    stage.dispose( );
    atlas.dispose( );
    font.dispose( );
  }

  // -- Private Methods -- \\
  private TextFieldListener getEnterListener( )
  {
    return new TextFieldListener( )
    {

      @Override
      public void keyTyped( TextField textField, char c )
      {
        if( c == '\r' )
        {
          connectToServer( );
        }
      }

    };
  }

  /**
   * method to send the connect to server {@link Message}
   */
  private void connectToServer( )
  {
    LoginData login = new LoginData( usernameField.getText( ), passwordField.getText( ) );
    FredStoneGame.client.connectToServer( login );
    // game.client.connectToServer( login );
  }

  /**
   * method to set the username and password in the {@link Login} screen
   * @param reset
   */
  public void setUsernamePassword( LoginData reset )
  {
    System.out.println( "set username / pw" );
    usernameField.setText( reset.getUsername( ) );
    passwordField.setText( reset.getPassword( ) );
  }

}
