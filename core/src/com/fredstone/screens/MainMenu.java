package com.fredstone.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Graphics.Monitor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fredstone.FredStoneGame;
import com.fredstone.control.MenuLabel;

public class MainMenu implements Screen
{
  public Texture img;

  private Stage stage;
  private TextureAtlas atlas;
  private Skin skin;
  private Table table;
  private TextButton startButton, testStartButton, collectionButton, optionButton, logoutButton;

  private MenuLabel playerName;

  private BitmapFont font;

  private final int HEIGHT = 475;

  // public TextField test;

  @Override
  public void show( )
  {

    Monitor monitor = Gdx.graphics.getMonitor( );
    DisplayMode displayMode = Gdx.graphics.getDisplayMode( monitor );
    // Gdx.graphics.setFullscreenMode(displayMode);

    stage = new Stage( );

    table = new Table( );
    Gdx.input.setInputProcessor( stage );

    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );
    skin = new Skin( atlas );

    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );

    playerName = new MenuLabel( FredStoneGame.client.getUser( ).getName( ) );
    playerName.getLabel( ).setPosition( 10, ( HEIGHT - playerName.getLabel( ).getHeight( ) * 2 ) );
    stage.addActor( playerName.getLabel( ) );

    TextButtonStyle style = new TextButtonStyle( );
    style.up = skin.getDrawable( "btn_up" );
    style.down = skin.getDrawable( "btn_down" );
    style.pressedOffsetX = 1;
    style.pressedOffsetY = -1;
    style.font = font;
    style.fontColor = Color.BLACK;
    style.downFontColor = Color.WHITE;

    font.getData( ).setScale( 0.5f );

    testStartButton = new TextButton( "TEST START", style );
    testStartButton.setSize( 125, 50 );

    testStartButton.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        // ScreenManager.getInstance( ).setScreen( new DeckSelection(
        // FredStoneGame.client.getPlayer( ) ) );
        // ScreenManager.getInstance( ).setScreen( new Pitch( ) );
        ScreenManager.getInstance( ).setScreen( new ConnectToRoom( ) );
        FredStoneGame.client.connectToRoom( );
      }

    } );

    startButton = new TextButton( "START", style );
    startButton.setSize( 125, 50 );

    startButton.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new DeckSelection( FredStoneGame.client.getUser( ) ) );
      }
    } );

    collectionButton = new TextButton( "COLLECTION", style );
    collectionButton.setSize( 125, 50 );

    collectionButton.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new CardCollection( FredStoneGame.client.getUser( ) ) );
      }
    } );

    optionButton = new TextButton( "OPTION", style );
    optionButton.setSize( 125, 50 );
    optionButton.addListener( new ClickListener( )
    {
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        // navigation
      }
    } );

    logoutButton = new TextButton( "LOGOUT", style );
    logoutButton.setSize( 125, 50 );

    logoutButton.addListener( new ClickListener( )
    {

      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new Login( ) );
      }
    } );

    // testButton.setSize( 100, 50 );
    // testButton.setPosition( 200, 250 );

    // testButton.pad( 20 );

    table.add( testStartButton );
    table.row( );
    table.add( startButton );
    table.row( );
    table.add( collectionButton );
    table.row( );
    table.add( optionButton );
    table.row( );
    table.add( logoutButton );

    table.setFillParent( true );

    stage.addActor( table );
  }

  @Override
  public void render( float delta )
  {
    Gdx.gl.glClearColor( 0, 0, 0, 1 );
    Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

    stage.act( delta );

    stage.draw( );

    // game.batch.begin( );
    // game.batch.draw( img, 0, 0 );
    // game.batch.end( );
  }

  @Override
  public void resize( int width, int height )
  {

  }

  @Override
  public void pause( )
  {

  }

  @Override
  public void resume( )
  {

  }

  @Override
  public void hide( )
  {

  }

  @Override
  public void dispose( )
  {
    img.dispose( );
    stage.dispose( );
    atlas.dispose( );
    font.dispose( );

  }

}
