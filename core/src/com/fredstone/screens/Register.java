package com.fredstone.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.fredstone.FredStoneGame;
import com.fredstone.model.LoginData;

public class Register implements Screen
{

  private final static int HEIGHT = 475;
  private final static int WIDTH = 800;
  private Stage stage;
  private Table table;
  private TextureAtlas atlas;
  private Skin skin;
  private BitmapFont font;
  private Label usernameLabel;
  private Label passwordLabel;
  private Label emailLabel;
  private TextField usernameField;
  private TextField passwordField;
  private TextField emailField;
  private TextButton registerButton;
  private Label registerText;

  public Label getRegisterText( )
  {
    return registerText;
  }

  public void setRegisterText( Label registerText )
  {
    this.registerText = registerText;
  }

  public TextButton getRegisterButton( )
  {
    return registerButton;
  }

  private TextButton backButton;
  private Texture backgroundTexture;

  public void show( )
  {
    stage = new Stage( );
    table = new Table( );
    Gdx.input.setInputProcessor( stage );
    backgroundTexture = new Texture( Gdx.files.internal( "login.png" ) );

    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );
    skin = new Skin( atlas );
    Skin testSkin = new Skin( Gdx.files.internal( "uiskin.json" ) );

    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).setScale( 0.3f );

    TextButtonStyle buttonStyle = new TextButtonStyle( );
    buttonStyle.up = skin.getDrawable( "btn_up" );
    buttonStyle.down = skin.getDrawable( "btn_down" );
    buttonStyle.pressedOffsetX = 1;
    buttonStyle.pressedOffsetY = -1;
    buttonStyle.font = font;
    buttonStyle.fontColor = Color.BLACK;
    buttonStyle.downFontColor = Color.WHITE;

    LabelStyle labelStyle = new LabelStyle( font, Color.WHITE );
    usernameLabel = new Label( "Username:", labelStyle );
    passwordLabel = new Label( "Password:", labelStyle );
    emailLabel = new Label( "Email:", labelStyle );

    TextFieldStyle fieldStyle = new TextFieldStyle( font, Color.WHITE, testSkin.getDrawable( "cursor" ), null, null );
    usernameField = new TextField( "", fieldStyle );
    passwordField = new TextField( "", fieldStyle );
    passwordField.setPasswordCharacter( '*' );
    passwordField.setPasswordMode( true );
    emailField = new TextField( "", fieldStyle );

    registerButton = new TextButton( "REGISTER", buttonStyle );
    registerButton.addListener( new ClickListener( )
    {

      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        registerButton.setTouchable( Touchable.disabled );
        String username = usernameField.getText( );
        String password = passwordField.getText( );
        String email = emailField.getText( );

        LoginData login = new LoginData( username, password, email );
        FredStoneGame.client.register( login );

      }

    } );

    backButton = new TextButton( "BACK", buttonStyle );
    backButton.addListener( new ClickListener( )
    {

      @Override
      public void clicked( InputEvent evt, float x, float y )
      {

        ScreenManager.getInstance( ).setScreen( new Login( ) );
      }

    } );
    registerText = new Label( "Registiert", labelStyle );
    registerText.setVisible( false );

    table.add( registerText );
    table.row( );
    table.add( usernameLabel );
    table.add( usernameField );

    table.row( );

    table.add( passwordLabel );
    table.add( passwordField );

    table.row( );

    table.add( emailLabel );
    table.add( emailField );

    table.row( );

    table.add( registerButton ).colspan( 2 );
    table.add( backButton ).colspan( 2 );

    // table.setFillParent( true );
    table.setDebug( true );
    table.debugTable( );
    // table.setPosition( WIDTH - table.getWidth(), HEIGHT - table.getHeight());
    table.setPosition( 200, 100 );
    stage.addActor( table );
  }

  @Override
  public void render( float delta )
  {
    Gdx.gl.glClearColor( 0, 0, 0, 1 );
    Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

    stage.act( delta );
    stage.getBatch( ).begin( );
    stage.getBatch( ).draw( backgroundTexture, 0, 0 );
    stage.getBatch( ).end( );

    stage.draw( );

  }

  @Override
  public void resize( int width, int height )
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void pause( )
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void resume( )
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void hide( )
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void dispose( )
  {
    // TODO Auto-generated method stub

  }

}
