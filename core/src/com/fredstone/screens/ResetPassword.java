package com.fredstone.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fredstone.FredStoneGame;
import com.fredstone.model.LoginData;
import com.fredstone.utils.Message;

public class ResetPassword implements Screen
{
  private Stage stage;
  private Table table;
  private Texture backgroundTexture;
  private TextureAtlas atlas;
  private Skin skin, uiSkin;
  private BitmapFont font;
  private Label usernameLabel;
  private TextField usernameField;
  private TextButton backBtn, acceptBtn;
  private TextButtonStyle buttonStyle;

  @Override
  public void show( )
  {
    stage = new Stage( );
    Gdx.input.setInputProcessor( stage );
    backgroundTexture = new Texture( Gdx.files.internal( "login.png" ) );
    
    table = new Table( );
    table.setFillParent( true );
    
    atlas = new TextureAtlas( Gdx.files.internal( "button.pack" ) );
    skin = new Skin( atlas );
    uiSkin = new Skin( Gdx.files.internal( "uiskin.json" ) );
    
    font = new BitmapFont( Gdx.files.internal( "font.fnt" ) );
    font.getData( ).setScale( 0.3f );
    
    LabelStyle labelStyle = new LabelStyle( font, Color.WHITE );
    usernameLabel = new Label( "Username:", labelStyle );
    
    TextFieldStyle fieldStyle = new TextFieldStyle( font, Color.WHITE, uiSkin.getDrawable( "cursor" ), null, null );
    usernameField = new TextField( "", fieldStyle );
    usernameField.setTextFieldListener( getEnterListener( ) );
    
    buttonStyle = new TextButtonStyle( );
    buttonStyle.up = skin.getDrawable( "btn_up" );
    buttonStyle.down = skin.getDrawable( "btn_down" );
    buttonStyle.pressedOffsetX = 1;
    buttonStyle.pressedOffsetY = -1;
    buttonStyle.font = font;
    buttonStyle.fontColor = Color.BLACK;
    buttonStyle.downFontColor = Color.WHITE;
    
    backBtn = new TextButton( "BACK", buttonStyle );
    backBtn.addListener( new ClickListener( ) {
      
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        ScreenManager.getInstance( ).setScreen( new Login( ) );
      }
    });
    
    acceptBtn = new TextButton( "RESET", buttonStyle );
    acceptBtn.addListener( new ClickListener( ) {
      
      @Override
      public void clicked( InputEvent evt, float x, float y )
      {
        resetPassword( );
      }
    });
    
    table.add( usernameLabel );
    table.add( usernameField );
    table.row( );
    table.add( backBtn );
    table.add( acceptBtn );
    
    stage.addActor( table );
    stage.setKeyboardFocus( usernameField );
  }

  @Override
  public void render( float delta )
  {
    Gdx.gl.glClearColor( 0, 0, 0, 1 );
    Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
    
    stage.act( delta );
    stage.getBatch( ).begin( );
    stage.getBatch( ).draw( backgroundTexture, 0, 0 );
    stage.getBatch( ).end( );
    stage.draw( );
  }

  @Override
  public void resize( int width, int height )
  {
    
  }

  @Override
  public void pause( )
  {
    
  }

  @Override
  public void resume( )
  {
    
  }

  @Override
  public void hide( )
  {
    
  }

  @Override
  public void dispose( )
  {
    stage.dispose( );
    atlas.dispose( );
    font.dispose( );
    skin.dispose( );
    uiSkin.dispose( );
  }
  
  /**
   * method to get the enter listener for {@link TextField}
   * @return {@link TextFieldListener}
   */
  private TextFieldListener getEnterListener( )
  {
    return new TextFieldListener( )
    {

      @Override
      public void keyTyped( TextField textField, char c )
      {
         if( c == '\r' )
         {
           resetPassword( );
         }
      }

    };
  }
  
  /**
   * method to send the reset password {@link Message}
   */
  private void resetPassword( )
  {
    LoginData reset = new LoginData( usernameField.getText( ), "123" );
    FredStoneGame.client.resetPW( reset );
    ScreenManager.getInstance( ).setScreen( new Login( ) );
  }

}
