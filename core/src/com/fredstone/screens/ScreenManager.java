package com.fredstone.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.fredstone.FredStoneGame;

public class ScreenManager
{
  private static ScreenManager instance;
  private FredStoneGame game;
  
  private ScreenManager( FredStoneGame game )
  {
    this.game = game;
  }
  
  /**
   * method to initialize the {@link ScreenManager}
   * @param game
   */
  public static void init( FredStoneGame game )
  {
    if( instance == null )
      instance = new ScreenManager( game );
  }
  
  /**
   * method to get the {@link ScreenManager} instance
   * @return
   */
  public static ScreenManager getInstance( )
  {
    return instance;
  }
  
  /**
   * method to set and show a {@link Screen}
   * @param screen
   */
  public void setScreen( final Screen screen )
  {
    Gdx.app.postRunnable( new Runnable( ) {

      @Override
      public void run( )
      {
        game.setScreen( screen );
      }
      
    });
  }
  
  /**
   * method to get the actual {@link Screen}
   * @return
   */
  public Screen getScreen(){
	  
	  return game.getScreen();
  }
}
