package com.fredstone.screens;

import com.fredstone.actor.CardActorGroup;

public interface ScreenOfCardgroup
{

  public CardActorGroup groupContainsPoint( float x, float y );
}
